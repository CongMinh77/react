import { BUY_FRUIT } from "./fruitTypes";

const initialState = {
    numberOfFruits: 50,
}

const fruitReducer = (state = initialState, action) => {
    switch (action.type) {
        case BUY_FRUIT:
            return {
                ... state,
                numberOfFruits: state.numberOfFruits - 2,
            }
    
        default:
            return state;
    }
}

export default fruitReducer;