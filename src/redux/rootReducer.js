import { combineReducers } from "redux";
import cakeReducer from "./cake/cakeReducer";
import fruitReducer from "./fruit/fruitReducer";
import iceCreamReducer from "./iceCream/iceCreamReducer";
import userReducer from "./user/UserReducer";

const rootReducer = combineReducers({
    cake: cakeReducer,
    iceCream: iceCreamReducer,
    fruit: fruitReducer,
    user: userReducer,
})

export default rootReducer