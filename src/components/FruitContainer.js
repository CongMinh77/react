import React from 'react'
import { connect } from 'react-redux'
import { buyFruit } from '../redux/fruit/fruitActions'
import { useSelector, useDispatch } from 'react-redux'

function FruitContainer() {
    const numberOfFruits = useSelector(state => state.fruit.numberOfFruits)
    const dispatch = useDispatch()
    
    return (
        <div>
            <h2>Number of Fruits - {numberOfFruits}</h2>
            <button onClick={() => dispatch(buyFruit())}>Buy Fruit</button>
        </div>
    )
}

export default FruitContainer
