import { useReducer } from "react";
import useFetch from "../hooks/useFetch";

const initialTodos = [
    {
      id: 1,
      title: "Todo 1",
      complete: false,
    },
    {
      id: 2,
      title: "Todo 2",
      complete: false,
    },
];

const reducer = (state, action) => {
    switch (action.type) {
        case "COMPLETE":
            return state.map((todo) => {
                if( todo.id === action.id )
                    return {...todo, complete: !todo.complete};
                else
                    return todo;
            })
        default:
            return state;
    }
};

function Example(){
    const [todos, dispatch] = useReducer(reducer, initialTodos);
    const [data] = useFetch("");

    const handleComplete = (todo) => {
        dispatch({type: "COMPLETE", id: todo.id});
    };

    return(
        <>
        {
            todos.map((todo) => (
                <div key={todo.id}>
                    <label>
                        <input type="checkbox"
                            checked={todo.complete}
                            onChange={() => handleComplete(todo)}
                        />
                        {todo.title}
                    </label>
                </div>
            ))
        }
        {
            data && data.map((item) => (
                <p key={item.id}>
                    {item.title}
                </p>
            ))
        }
        </>
    );
}

export default Example;