import { useState, useEffect, useRef } from "react";

function Ref() {
    const [inputValue, setInputValue] = useState("");
    const previousInputValue = useRef("");

    useEffect(() => {
        previousInputValue.current = inputValue;
    }, [inputValue]);

    return(
        <>
            <input type="text" value={inputValue} 
                onChange={(e)=>setInputValue(e.target.value)}/>
            <h3>Current Value: {inputValue}</h3>
            <h3>Previous Value: {previousInputValue.current}</h3>
        </>
    );
}

export default Ref;