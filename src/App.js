import logo from './logo.svg';
import { Provider } from 'react-redux';
import { useState } from 'react';
import store from './redux/store';
import './App.css';
import { buyCake } from './redux';
import CakeContainer from './components/CakeContainer';
import HooksCakeContainer from './components/HooksCakeContainer';
import IceCreamContainer from './components/IceCreamContainer';
import FruitContainer from './components/FruitContainer';
import NewCakeContainer from './components/NewCakeContainer';
import ItemContainer from './components/ItemContainer';
import UserContainer from './components/UserContainer';

function App() {

  return (
    <Provider store={store}>
      <div className="App">
        <header className="App-header">
          <UserContainer></UserContainer>
          {/* <ItemContainer cake></ItemContainer>
          <ItemContainer></ItemContainer>
          <IceCreamContainer></IceCreamContainer>
          <CakeContainer></CakeContainer>
          <FruitContainer></FruitContainer>
          <NewCakeContainer></NewCakeContainer> */}
          {/* <HooksCakeContainer></HooksCakeContainer> */}
        </header>
      </div>
    </Provider>
  );
}

export default App;
