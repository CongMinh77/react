import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import MyForm from './components/Form';
import Ref from './components/StageChange';
import Example from './components/Example';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    {/* <Example /> */}
    <App />
    {/* <MyForm /> */}
    {/* <Ref /> */}
  </React.StrictMode>,
  document.getElementById('root')
);

/**
 * If you want to start measuring performance in your app, pass a function
 * to log results (for example: reportWebVitals(console.log))
 * or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
 */
reportWebVitals();
